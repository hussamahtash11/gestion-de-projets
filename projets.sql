-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 17 déc. 2021 à 16:51
-- Version du serveur :  8.0.27-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestion_de_projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

CREATE TABLE `projets` (
  `id` int NOT NULL,
  `nom_proj` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `desc_proj` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `projets`
--

INSERT INTO `projets` (`id`, `nom_proj`, `desc_proj`) VALUES
(3, ' Equipe de Football de FC Toulouse ', 'C`est un club de football français fondé en 1970 dans la ville de Toulouse, et surnommé  Il a commencé à jouer professionnellement en 1979 et a remporté le titre de deuxième division à deux reprises. Il joue pour la première fois en première division en 1982 et élimine l`équipe italienne de Naples de la Ligue européenne en 1986. Il termine la saison 2006-2007 à la troisième place de la première division française et participe aux éliminatoires de la Ligue européenne des champions. la saison suivante, pour la première fois de son histoire. Dans ses rangs se trouvaient le gardien français Fabien Barthez, champion du monde 1998, et le français André-Pierre Gignac.'),
(4, 'Equipe de Football de Juventus', 'La Juventus de Turin est un club historique du championnat de football italien, fondé en 1897 par un groupe d`étudiants turinois. Lié à la puissante famille Agnelli depuis les années 1920, ce club de football est devenu une institution en Italie. La Juve dispute ses rencontres à l`Allianz Stadium de Turin, une enceinte ultra moderne de 41 507 places. Avec 35 titres de Champion d`Italie, 13 Coupes d`Italie, 2 Ligues des Champions et 3 coupes de l`UEFA, une Coupe des Coupes et deux coupes intercontinentales, les Bianconeri dispose d`un palmarès unique en Italie et parmi les plus riches de la planète football.'),
(5, 'Equipe de Football de FC Barcelone ', 'Le FC Barcelone sur Foot Mercato Le FC Barcelone, plus communément appelé le Barça, est un club phare du championnat Espagnol de football depuis sa création en 1899. Symbole d`une région indépendantiste, la Catalogne, les Blaugranas ont pour éternel rival le club de la capitale, le Real Madrid.  Qui dit club renommé dit grande actualité dans les médias locaux et internationaux. Avec deux titres de presse qui couvrent en grande partie les infos des Catalans, respectivement Sport et El Mundo Deportivo, chaque jour réserve ses surprises aussi bien sur le terrain qu`en extra sportif.'),
(6, 'Chelsea Football Club', 'Cet article présente l histoire du club de football de Chelsea. Fondé en 1905, Chelsea acquiert rapidement une réputation dans le football anglais par son recrutement de joueurs de renom et par son stade faisant régulièrement guichets fermés. Toutefois, le club ne parvient pas à remporter de trophée pour ses cinquante premières années. Chelsea passe trente de ses quarante premières saisons en première division, mais finit régulièrement en milieu de tableau voire évite la relégation. Le premier succès majeur de Chelsea est sa qualification en finale de la FA Cup en 1915 (battu par Sheffield United). Le club a également atteint les demi-finales de cette compétition en 1911, 1920, 1932, 1950 et 1952. La malchance de Chelsea s`achève enfin sous la houlette de l`entraîneur Ted Drake qui, après des changements fondamentaux au club, mène Chelsea à son premier titre de champion en 1955.'),
(7, 'PSG Football Club', 'Le PSG, un jeune club Le Paris Saint-Germain Football Club a été fondé en 1970 suite à la fusion du Paris FC et du Stade Saint-Germain. Le PSG évolue à domicile au Parc des Princes, un stade d`une capacité de 47 929 places. Des travaux sont à l étude afin de permettre de supporter les forts affluences récurrentes depuis quelques années.  Un effectif de stars au Paris SG Depuis le rachat du club par le Qatar, le PSG a recruté certaines des plus grandes stars de la planète football et a annoncé vouloir poursuivre cette politique de transfert lors des prochains mercato.  Paris a enchaîné les propriétaires Le club de la Capitale a la particularité d avoir connu de nombreux actionnaires ou propriétaires. Ainsi, le Paris SG, après la période Daniel Hechter, a été la propriété du groupe audiovisuel Canal +, du fond d investissement Colony Capital et depuis 2011 du fond qatari Qatar Investment Authority.');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `projets`
--
ALTER TABLE `projets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `projets`
--
ALTER TABLE `projets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
